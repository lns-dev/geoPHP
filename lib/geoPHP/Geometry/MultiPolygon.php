<?php
/**
 * MultiPolygon: A collection of Polygons
 */
namespace geoPHP\Geometry;

class MultiPolygon extends Collection 
{
  protected $geom_type = 'MultiPolygon';
}
